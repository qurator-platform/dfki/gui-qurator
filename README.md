Gui for Qurator Demo at 4th September 2019, Berlin

Author: Ela Elsholz (DFKI)
Date: 19-08-19

Description: 
Simple GUI that lists all services and links to them

For more info how to create a service UI and add it to the overview ui,
see the PDF documentation in the repo.