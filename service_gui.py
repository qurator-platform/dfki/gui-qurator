def get_html_file_name(srv_name):
    switcher = {
        'topicdetection': "services/topic-detection.html",
        'namedentityrecognition': "services/named-entity-recognition.html",
        'timeexpressionextraction': "services/time-expression-extraction.html",
        'summarisation': "services/summarisation.html",
        'languageidentification': "services/language-identification.html",
        'postagging': "services/pos-tagging.html",
        'sentimentanalysis': "services/sentiment-analysis.html",
        'legalentityrecognition': "services/legal-entity-recognition.html",
        'shakespearechatbot': "services/shakespeare-chatbot.html",
    }
    return switcher.get(srv_name, "index.html")

def get_headline(srv_name):
    switcher = {
        'topicdetection': "Topic Detection",
        'namedentityrecognition': "Named Entity Recognition",
        'timeexpressionextraction': "Time Expression Extraction",
        'summarisation': "Summarisation",
        'languageidentification': "Language Identification",
        'postagging': "POS Tagging",
        'sentimentanalysis': "Sentiment Analysis",
        'legalentityrecognition': "Legal Entity Recognition",
        'shakespearechatbot': "Shakespeare Chatbot",
    }
    return switcher.get(srv_name, "Teilprojekt DFKI: Ausgewählte Services")