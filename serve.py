# serve.py
from flask import Flask
from flask import render_template
from flask import request

import service_gui as gui

# creates a Flask application, named app
app = Flask(__name__)

# a route where we will display a welcome message via an HTML template
@app.route("/", methods=['POST','GET'])
def qurator():
    headline = gui.get_headline('default')
    return render_template('index.html', headline=headline)

@app.route("/service", methods=['POST','GET'])
def service():
    service_name = request.args.get('name')
    headline = gui.get_headline(service_name)
    data = {
        'headline': headline,
    }
    srv_html_file = gui.get_html_file_name(service_name)
    return render_template(srv_html_file, **data)

# run the application
if __name__ == "__main__":
    app.run(debug=True)