from python:3

COPY serve.py .
COPY service_gui.py .
COPY static static/
COPY templates templates/

RUN pip install flask

EXPOSE 8080
ENTRYPOINT FLASK_APP=serve.py flask run --host=0.0.0.0 --port=8080